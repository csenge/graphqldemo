package edu.csenge.GraphQLDemo.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import edu.csenge.GraphQLDemo.entity.Pet;
import edu.csenge.GraphQLDemo.enums.Animal;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {
    List<Pet> pets = new ArrayList<>();

    public List<Pet> pets() {

        Pet aPet = new Pet();
        aPet.setId(1l);
        aPet.setName("Bill");
        aPet.setAge(9);
        aPet.setType(Animal.MAMMOTH);

        pets.add(aPet);

        Pet bPet = new Pet();
        bPet.setId(2l);
        bPet.setName("Ava");
        bPet.setAge(13);
        bPet.setType(Animal.DOG);

        pets.add(bPet);


        return pets;
    }

    public Pet add(String name) {
        Pet pet = new Pet();
      //  pet.setId(id);
      pet.setName(name);
      //  pet.setAge(age);
        pets.add(pet);
        return pet;
    }
}
