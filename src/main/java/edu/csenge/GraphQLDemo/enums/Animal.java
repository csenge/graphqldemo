package edu.csenge.GraphQLDemo.enums;

public enum Animal {
    DOG,
    CAT,
    BADGER,
    MAMMOTH
}
